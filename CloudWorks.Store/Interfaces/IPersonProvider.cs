﻿using CloudWorks.Store.BaseClasses;

namespace CloudWorks.Store.Interfaces
{
    /// <summary>
    /// Interface define functionality of getting persons.
    /// </summary>
    public interface IPersonProvider
    {
        /// <summary>
        /// Rertieve <see cref="Person"/>
        /// </summary>
        /// <returns>Return <see cref="Person"/></returns>
        Person[] GetPersons();
    }
}
