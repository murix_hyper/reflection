﻿using CloudWorks.Store.Attributes;
using System.Linq;
using System.Reflection;

namespace CloudWorks.Store.Utils
{
    public class Validator
    {
        public static bool IsValid<T>(T person)
        {
            // TODO: make a cache.
            var propAttributes = typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public)
                                      .ToDictionary(pi => pi, pi => pi.GetCustomAttributes());

            foreach (var propAttr in propAttributes)
            {
                foreach (var attr in propAttr.Value)
                {
                    // TODO: implement additional base interface of validation attributes.
                    if (attr is StringLengthAttribute)
                    {
                        var sla = (StringLengthAttribute)attr;
                        string value = propAttr.Key.GetValue(person) as string;
                        
                        if (sla.Length < value.Length)
                        {
                            return false;
                        }
                    }
                }
            }
            
            return true;
        }
    }
}
