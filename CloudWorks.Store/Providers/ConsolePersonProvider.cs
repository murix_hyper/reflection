﻿using CloudWorks.Store.BaseClasses;
using CloudWorks.Store.Classes;
using CloudWorks.Store.Interfaces;
using CloudWorks.Store.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudWorks.Store.Providers
{
    /// <inheritdoc/>
    public class ConsolePersonProvider : IPersonProvider
    {
        /// <inheritdoc/>
        public Person[] GetPersons()
        {
            int personsCount = 0;
            while (personsCount == 0)
            {
                Console.WriteLine("Введіть загальну кількість людей в магазині");
                int.TryParse(Console.ReadLine(), out personsCount);
            }
            var persons = new Person[personsCount];

            for (var i = 0; i < personsCount; i++)
            {
                Console.WriteLine("Введіть ім'я");
                var name = Console.ReadLine();

                Console.WriteLine("Якщо ви хочете додати працівника введіть \"1\". Якщо ви хочете додати покупця введіть \"2\"");
                var personType = Console.ReadLine();

                if (personType == "1")
                {
                    Console.WriteLine($"Швидкість продажу працівником відрізняється від {Employee.DefaultCheckoutSpeed}? т(так)/н(ні)");
                    var isCheckoutSpeedDifferent = Console.ReadLine();
                    if (isCheckoutSpeedDifferent == "т")
                    {
                        Console.WriteLine("Введіть швидкість продажу співробітником");
                        var speedStr = Console.ReadLine();
                        if (double.TryParse(speedStr, out var speed))
                        {
                            var person = new Employee(name, speed);

                            if (!Validator.IsValid(person))
                            {
                                Console.WriteLine("Дані не пройшли валідацію. Повторіть ввід.");
                                i--;
                                continue;
                            }

                            persons[i] = person;
                        }
                        else
                        {
                            Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                            i--;
                            continue;
                        }
                    }
                    else if (isCheckoutSpeedDifferent == "н")
                    {
                        var person = new Employee(name);

                        if (!Validator.IsValid(person))
                        {
                            Console.WriteLine("Дані не пройшли валідацію. Повторіть ввід.");
                            i--;
                            continue;
                        }

                        persons[i] = person;
                    }
                    else
                    {
                        Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                        i--;
                        continue;
                    }
                }
                else if (personType == "2")
                {
                    Console.WriteLine($"Швидкість покупки клієнтом відрізняється від {Client.DefaultCheckoutSpeed}? т(так)/н(ні)");
                    var isCheckoutSpeedDifferent = Console.ReadLine();
                    if (isCheckoutSpeedDifferent == "т")
                    {
                        Console.WriteLine("Введіть швидкість покупки клієнтом");
                        var speedStr = Console.ReadLine();
                        if (double.TryParse(speedStr, out var speed))
                        {
                            var client = new Client(name, speed);
                            client.Items = ReadProductList().ToArray();

                            if (!Validator.IsValid(client))
                            {
                                Console.WriteLine("Дані не пройшли валідацію. Повторіть ввід.");
                                i--;
                                continue;
                            }

                            persons[i] = client;
                        }
                        else
                        {
                            Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                            i--;
                            continue;
                        }
                    }
                    else if (isCheckoutSpeedDifferent == "н")
                    {
                        var client = new Client(name);
                        client.Items = ReadProductList().ToArray();

                        if (!Validator.IsValid(client))
                        {
                            Console.WriteLine("Дані не пройшли валідацію. Повторіть ввід.");
                            i--;
                            continue;
                        }

                        persons[i] = client;
                    }
                    else
                    {
                        Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                        i--;
                        continue;
                    }
                }
                else
                {
                    Console.WriteLine("Ви ввели не вірні дані. Повторіть ввід.");
                    i--;
                    continue;
                }
            }

            return persons;
        }
        public IEnumerable<Product> ReadProductList()
        {
            var choise = "т";
            while (choise == "т")
            {
                Console.WriteLine("Введіть назву продукта");
                yield return new Product { Name = Console.ReadLine() };
                Console.WriteLine("Продовжити заповнення корзини? т(так)");
                choise = Console.ReadLine();
            }
        }
    }
}
